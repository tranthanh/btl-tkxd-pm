/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructure;

import java.util.HashMap;
import java.util.TreeMap;

/**
 *
 * @author MR_THANH
 */
public class TwoLevelHashMap<T1, T2, T3> extends HashMap<T1, HashMap<T2, T3>> {

    private HashMap<T1, HashMap<T2, T3>> data;

    public TwoLevelHashMap() {
        this.data = new HashMap<>();
    }

    public void put(T1 key, T2 mediate, T3 value) {
        HashMap<T2, T3> mapValue = data.get(key);
        if (mapValue == null) {
            mapValue = new HashMap();
            mapValue.put(mediate, value);
            data.put(key, mapValue);
            System.out.println("them 1");
        } else {
            mapValue.put(mediate, value);
             System.out.println("them 2");
        }
    }
}
