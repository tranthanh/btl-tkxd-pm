/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.Date;

/**
 *
 * @author MR_THANH
 */
public class Book {

    private int id;
    private String bookCode;
    private String bookName;
    private String bookCover;
    private String publisher;
    private String isbn;
    private String bookType;
    private Date enterTime;
    private int enterUser;

    public Book(int id, String bookCode, String bookName, String bookCover, String publisher, String isbn, String bookType, Date enterTime, int enterUser) {
        this.id = id;
        this.bookCode = bookCode;
        this.bookName = bookName;
        this.bookCover = bookCover;
        this.publisher = publisher;
        this.isbn = isbn;
        this.bookType = bookType;
        this.enterTime = enterTime;
        this.enterUser = enterUser;
    }

    public String getBookCode() {
        return bookCode;
    }

    public int getId() {
        return id;
    }

    public String getBookName() {
        return bookName;
    }

    public String getBookCover() {
        return bookCover;
    }

    public String getPublisher() {
        return publisher;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getBookType() {
        return bookType;
    }

    public Date getEnterTime() {
        return enterTime;
    }

    public int getEnterUser() {
        return enterUser;
    }

}
