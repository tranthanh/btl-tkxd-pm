/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author MR_THANH
 */
public class Function implements Comparable<Function> {

    private String functionName;
    private String functionCode;
    private int displayOrder;

    public Function(String functionName, String functionCode, int displayOrder) {
        this.functionName = functionName;
        this.functionCode = functionCode;
        this.displayOrder = displayOrder;
    }

    public String getFunctionName() {
        return functionName;
    }

    public String getFunctionCode() {
        return functionCode;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    @Override
    public int compareTo(Function other) {
        return Integer.compare(this.getDisplayOrder(), other.getDisplayOrder());
    }

}
