/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author MR_THANH
 */
public class RegisterItem {
    private int registerId;
    private int borrowId;
    private int returnId;
    private int copyId;
    private String registerStatus;

    public RegisterItem(int registerId, int borrowId, int returnId, int copyId, String registerStatus) {
        this.registerId = registerId;
        this.borrowId = borrowId;
        this.returnId = returnId;
        this.copyId = copyId;
        this.registerStatus = registerStatus;
    }

    public int getRegisterId() {
        return registerId;
    }

    public int getBorrowId() {
        return borrowId;
    }

    public int getReturnId() {
        return returnId;
    }

    public int getCopyId() {
        return copyId;
    }

    public String getRegisterStatus() {
        return registerStatus;
    }
    
}
