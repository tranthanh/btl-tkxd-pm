/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.List;

/**
 *
 * @author MR_THANH
 */
public class StudentInfo extends UserInfo {

    private String studentCode;
    private String classStudent;
    private int enrolledYear;

    public StudentInfo(int id, String userType, String username, String password,
            String fullName, String email, String facebookId, String googleId,
            String gender, String phoneNumber,
            String studentCode, String classStudent, int enrolledYear) {
        super(id, userType, username, password, fullName, email, facebookId, googleId, gender, phoneNumber);
        this.studentCode = studentCode;
        this.classStudent = classStudent;
        this.enrolledYear = enrolledYear;
    }

    public String getStudentCode() {
        return studentCode;
    }

    public void setStudentCode(String studentCode) {
        this.studentCode = studentCode;
    }

    public String getClassStudent() {
        return classStudent;
    }

    public void setClassStudent(String classStudent) {
        this.classStudent = classStudent;
    }

    public int getEnrolledYear() {
        return enrolledYear;
    }

    public void setEnrolledYear(int enrolledYear) {
        this.enrolledYear = enrolledYear;
    }

    
}
