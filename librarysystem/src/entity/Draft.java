/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.Comparator;

/**
 *
 * @author MR_THANH
 */
public class Draft implements Comparable<Draft>, Comparator<Draft> {

    private int a;
    private int b;

    public Draft(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + this.a;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        return true;

    }

    @Override
    public int compareTo(Draft other) {
        if (this.getA() > other.getA()) {
            return 1;
        } else if (this.getA() < other.getA()) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public int compare(Draft o1, Draft o2) {
        return o1.getA() - o2.getA();
    }

}
