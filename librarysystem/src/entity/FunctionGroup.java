/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.List;
import java.util.Objects;

/**
 *
 * @author MR_THANH
 */
public class FunctionGroup implements Comparable<FunctionGroup> {

    private String groupName;
    private String groupCode;
    private int displayOrder;
    private String side;
    private List<Function> funtions;

    public FunctionGroup(String groupName, String groupCode, int displayOrder, String side, List<Function> funtions) {
        this.groupName = groupName;
        this.groupCode = groupCode;
        this.displayOrder = displayOrder;
        this.side = side;
        this.funtions = funtions;
    }

    public String getGroupName() {
        return groupName;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public List<Function> getFuntions() {
        return funtions;
    }

    public String getSide() {
        return side;
    }
    
    

    @Override
    public int compareTo(FunctionGroup other) {
        return Integer.compare(displayOrder, other.displayOrder);
    }

}
