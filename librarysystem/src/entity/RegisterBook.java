/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.Date;
import java.util.List;

/**
 *
 * @author MR_THANH
 */
public class RegisterBook {
    private Date register_time;
    private int register_user;
    private List<Book> books;

    public RegisterBook(Date register_time, int register_user, List<Book> books) {
        this.register_time = register_time;
        this.register_user = register_user;
        this.books = books;
    }

    public Date getRegister_time() {
        return register_time;
    }

    public int getRegister_user() {
        return register_user;
    }

    public List<Book> getBooks() {
        return books;
    }
}
