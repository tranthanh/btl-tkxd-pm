/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import com.sun.javafx.scene.control.skin.VirtualFlow;
import datastructure.TwoLevelHashMap;
import entity.Book;
import entity.Function;
import entity.FunctionGroup;
import entity.StudentInfo;
import entity.UserInfo;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MR_THANH
 */
public class DatabaseHelper {

    private static String dbUsername = "sa";
    private static String dbPassword = "sa123456";
    private static String dbUrl = "jdbc:sqlserver://localhost:1433;databaseName=library_system";
    private static Connection conn = null;
    private static int connectCounter = 0;

    private static Connection openDatabase() {
        try {
            if (conn == null || conn.isClosed()) {
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                conn = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
                connectCounter = 1;
            } else {
                connectCounter++;
            }
            return conn;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void closeDatabase() {
        try {
            if (connectCounter > 0) {
                conn.close();
                connectCounter = 0;
            } else {
                connectCounter--;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static List<FunctionGroup> getUserFunctionGroup(int userId) {
        try {
            String sql = "select fg.*, fg.display_order as group_order , ft.function_name, ft.function_code, ft.display_order as function_order\n"
                    + "from (select * from funtion_user where users_id = ?) as fu\n"
                    + "	left join functions as ft on fu.function_id = ft.id\n"
                    + "	left join function_group as fg on ft.group_id = fg.id";
            Connection connect = openDatabase();
            PreparedStatement ps = connect.prepareStatement(sql);
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            HashMap<String, FunctionGroup> funcGroups = new HashMap<>();
            while (rs.next()) {
                String groupName = rs.getString("group_name");
                String groupCode = rs.getString("group_code");
                int groupOrder = rs.getInt("group_order");
                String groupSide = rs.getString("side");
                String functionName = rs.getString("function_name");
                String functionCode = rs.getString("function_code");
                int functionOrder = rs.getInt("function_order");
                Function function = new Function(functionName, functionCode, functionOrder);
                if (funcGroups.get(groupCode) == null) {
                    List<Function> functions = new ArrayList<Function>(20);
                    functions.add(function);
                    FunctionGroup funcGroup = new FunctionGroup(groupName, groupCode, groupOrder, groupSide, functions);
                    funcGroups.put(groupCode, funcGroup);
                } else {
                    List<Function> list = funcGroups.get(groupCode).getFuntions();
                    list.add(function);
                }
            }
            return new ArrayList<FunctionGroup>(funcGroups.values());
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static UserInfo getUserInfo(String username, String password) {
        try {
            String sql = "select us.*, st.student_code, st.class_student, st.enrolled_year\n"
                    + "from (select * from users where username=? and acc_password =? ) as us\n"
                    + " left join students as st on us.id = st.users_id";
            Connection connect = openDatabase();
            PreparedStatement ps = connect.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int userId = rs.getInt("id");
                String userType = rs.getString("user_type");
                String username2 = rs.getString("username");
                String password2 = rs.getString("acc_password");
                String fullName = rs.getString("full_name");
                String email = rs.getString("email");
                String facebookId = rs.getString("facebook_id");
                String googleId = rs.getString("google_id");
                String gender = rs.getString("gender");
                String phoneNumber = rs.getString("phone_number");
                if (userType.equals("student")) {
                    String studentCode = rs.getString("student_code");
                    String classStudent = rs.getString("class_student");
                    int enrolledYear = rs.getInt("enrolled_year");
                    UserInfo userInfo = new StudentInfo(userId, userType, username2,
                            password2, fullName, email, facebookId, googleId,
                            gender, phoneNumber, studentCode,
                            classStudent, enrolledYear);
                    return userInfo;
                } else {
                    return null;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static Book getBookInfo(int bookId) {
        try {
            String sql = "select * from book where id = ?";
            Connection connect = openDatabase();
            PreparedStatement ps = connect.prepareStatement(sql);
            ps.setInt(1, bookId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int bookId1 = rs.getInt("id");
                String bookCode = rs.getString("book_code");
                String bookName = rs.getString("book_name");
                String bookCover = rs.getString("book_cover");
                String publisher = rs.getString("publisher");
                String isbn = rs.getString("isbn");
                String bookType = rs.getString("book_type");
                Date enterTime = rs.getDate("enter_time");
                int enterUser = rs.getInt("enter_user");
                Book book = new Book(bookId, bookCode, bookName, bookCover, publisher, isbn, bookType, enterTime, enterUser);
                return book;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static int getRegisteredBook(int userId) {
        return 5;
    }

    public static List<Book> registerBook(List<Book> books, UserInfo user) {
        try {
            java.sql.Date currentTime = new java.sql.Date(new Date().getTime());
            String sql = "insert into register_book(register_time, register_user)"
                    + "values(?,?)";
            Connection connect = openDatabase();
            PreparedStatement ps = connect.prepareStatement(sql);
            ps.setDate(1, currentTime);
            ps.setInt(2, user.getId());
            ps.execute();
            sql = "select * from register_book where register_time = ? and register_user = ?";
            ps = connect.prepareStatement(sql);
            ps.setDate(1, currentTime);
            ps.setInt(2, user.getId());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int registerId = rs.getInt("id");
                System.out.print(registerId);
                for (Book book : books) {
                    sql = "select * from book_copy where book_id = ? and copy_status = ?";
                    ps = connect.prepareStatement(sql);
                    ps.setInt(1, book.getId());
                    ps.setString(2, "available");
                    rs = ps.executeQuery();
                    if (rs.next()) {
                        int copyId = rs.getInt("id");
                        sql = "insert into register_item(register_id, copy_id) values(?, ?)";
                        ps = connect.prepareStatement(sql);
                        ps.setInt(1, registerId);
                        ps.setInt(2, copyId);
                        ps.execute();
                        sql = "update book_copy set copy_status = ?";
                        ps = connect.prepareStatement(sql);
                        ps.setString(1, "registering");
                        ps.execute();
                    }
                }
            }
            sql = "select b.* from (select * from register_book where register_user = ? ) as rb \n"
                    + "left join register_item as ri on rb.id = ri.register_id\n"
                    + "left join book_copy as bc on ri.copy_id = bc.id\n"
                    + "left join book as b on bc.book_id = b.id";
            ps = connect.prepareStatement(sql);
            ps.setInt(1, user.getId());
            rs = ps.executeQuery();
            List<Book> regisBooks = new ArrayList<>(50);
            while (rs.next()) {
                int bookId = rs.getInt("id");
                String bookCode = rs.getString("book_code");
                String bookName = rs.getString("book_name");
                String bookCover = rs.getString("book_cover");
                String publisher = rs.getString("publisher");
                String isbn = rs.getString("isbn");
                String bookType = rs.getString("book_type");
                Date enterTime = rs.getDate("enter_time");
                int enterUser = rs.getInt("enter_user");
                Book book = new Book(bookId, bookCode, bookName, bookCover, publisher, isbn, bookType, enterTime, enterUser);
                regisBooks.add(book);
            }
            closeDatabase();
            return regisBooks;
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ArrayList<>();
    }
}
