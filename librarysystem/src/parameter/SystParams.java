/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parameter;

import entity.Book;
import entity.UserInfo;
import graphic.MainFrame;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import utility.DatabaseHelper;

/**
 *
 * @author MR_THANH
 */
public class SystParams {

    private static List<Book> bookCart;
    private static UserInfo curruentUser;
    private static MainFrame mainFrame;

    public static List<Book> getBookCart() {
        if (bookCart == null) {
            return new ArrayList<>(40);
        }
        return bookCart;
    }

    public static UserInfo getCurruentUser() {
        return curruentUser;
    }

    public static void setCurruentUser(UserInfo curruentUser) {
        SystParams.curruentUser = curruentUser;
    }
    

    public static void addBookToCart(int bookId) {
        if (bookCart == null) {
            bookCart = new ArrayList<>(40);
        }
        Book book = DatabaseHelper.getBookInfo(bookId);
        System.out.println("trying to add " + bookId);
        if (book != null) {
            bookCart.add(book);
            for (Book item : bookCart) {
                System.out.println("added " + item.getBookName());
            }
        }
    }

    public static MainFrame getMainFrame() {
        return mainFrame;
    }

    public static void setMainFrame(MainFrame mainFrame) {
        SystParams.mainFrame = mainFrame;
    }

}
