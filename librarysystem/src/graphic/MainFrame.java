/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphic;

import entity.Function;
import entity.FunctionGroup;
import entity.UserInfo;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.web.WebView;
import javax.swing.Box;
import javax.swing.JDialog;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import panel.CardPanel;
import panel.HomePanel;
import panel.RegisterBookPanel;
import parameter.SystParams;
import sun.awt.WindowClosingListener;
import utility.DatabaseHelper;

/**
 *
 * @author MR_THANH
 */
public class MainFrame extends javax.swing.JFrame {

    private UserInfo userInfo;
    private List<FunctionGroup> funcGroups;

    public void showCard(String name) {
        ((CardLayout) (cards.getLayout())).show(cards, name);
        for (Component comp : cards.getComponents()) {
            if (comp.isVisible() == true) {
                if(comp instanceof CardPanel){
                    ((CardPanel)comp).updateData();
                }
            }
        }
    }
    
    public void addCardIfNotExist(JPanel panel, String name){
        cards.add(panel, name);
    }

    public void login() {
//        JFXPanel jfxPanel = new JFXPanel();
//        jdlWebview.add(jfxPanel);
//        Platform.runLater(() -> {
//            WebView webView = new WebView();
//            jfxPanel.setScene(new Scene(webView));
//            webView.getEngine().load("http://www.stackoverflow.com/");
//        });
//        jdlWebview.setVisible(true);

        WindowListener exitListener = new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        };
        jdlLogin.addWindowListener(exitListener);
        jdlLogin.setLocationRelativeTo(null);
        jdlLogin.setVisible(true);
        initElements(funcGroups);
        this.setVisible(true);
    }

    public void logout() {
        this.setVisible(false);
        login();
    }

//    public void initElements() {
//        cards.add(new HomePanel(), "HomePanel");
//        cards.add(new RegisterBookPanel(), "RegisterBookPanel");
//    }
    private void initElements(List<FunctionGroup> funcGroups) {
        Collections.sort(funcGroups);
        List<JMenu> leftMenu = new ArrayList<>(20);
        List<JMenu> rightMenu = new ArrayList<>(20);
//        JMenu fileMenu = new JMenu("File");
//        JMenuItem exitItem = new JMenuItem("Exit");
//        exitItem.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                logout();
//            }
//        });
//        leftMenu.add(fileMenu);
//        fileMenu.add(exitItem);
        for (FunctionGroup group : funcGroups) {
            Collections.sort(group.getFuntions());
            JMenu menu = new JMenu(group.getGroupName());
            Iterator iter = group.getFuntions().iterator();
            while (iter.hasNext()) {
                try {
                    Function func = (Function) iter.next();
                    JMenuItem item = new JMenuItem(func.getFunctionName());
                    Class<? extends JPanel> cla = Class.forName(func.getFunctionCode()).asSubclass(JPanel.class);
                    JPanel panel = cla.newInstance();
                    cards.add(panel, func.getFunctionCode());
                    item.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            System.out.println("showing" + func.getFunctionCode());
                            ((CardLayout) cards.getLayout()).show(cards, func.getFunctionCode());
                        }
                    });
                    menu.add(item);
                    if (iter.hasNext()) {
                        menu.addSeparator();
                    }
                } catch (ClassNotFoundException ex) {
                    //Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InstantiationException ex) {
                    // Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    //Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (group.getSide().equals("left")) {
                leftMenu.add(menu);
            } else if (group.getSide().equals("right")) {
                rightMenu.add(menu);
            }
        }
        jMenuBar.removeAll();
        for (JMenu menu : leftMenu) {
            jMenuBar.add(menu);
        }
        jMenuBar.add(Box.createHorizontalGlue());
        for (JMenu menu : rightMenu) {
            jMenuBar.add(menu);
        }
    }

    /**
     * Creates new form MainFrame
     */
    public MainFrame() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jdlLogin = new javax.swing.JDialog(this, "", JDialog.DEFAULT_MODALITY_TYPE.APPLICATION_MODAL);
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jtfUsername = new javax.swing.JTextField();
        jpfPassword = new javax.swing.JPasswordField();
        jbtLogin = new javax.swing.JButton();
        jlbError = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jdlWebview = new javax.swing.JDialog();
        cards = new javax.swing.JPanel();
        jMenuBar = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();

        jdlLogin.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        jdlLogin.setTitle("Library system");
        jdlLogin.setBounds(new java.awt.Rectangle(0, 0, 500, 300));
        jdlLogin.setResizable(false);

        jLabel3.setText("Tài khoản");
        jLabel3.setName(""); // NOI18N

        jLabel4.setText("Mật khẩu");

        jtfUsername.setText("student");

        jpfPassword.setText("password");

        jbtLogin.setText("Đăng nhập");
        jbtLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtLoginActionPerformed(evt);
            }
        });

        jlbError.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jlbError.setForeground(new java.awt.Color(255, 0, 0));
        jlbError.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlbError.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        jButton1.setText("Đăng ký");

        javax.swing.GroupLayout jdlLoginLayout = new javax.swing.GroupLayout(jdlLogin.getContentPane());
        jdlLogin.getContentPane().setLayout(jdlLoginLayout);
        jdlLoginLayout.setHorizontalGroup(
            jdlLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jdlLoginLayout.createSequentialGroup()
                .addGap(72, 72, 72)
                .addGroup(jdlLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jlbError, javax.swing.GroupLayout.PREFERRED_SIZE, 285, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jdlLoginLayout.createSequentialGroup()
                        .addGroup(jdlLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4))
                        .addGap(18, 18, 18)
                        .addGroup(jdlLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jtfUsername, javax.swing.GroupLayout.DEFAULT_SIZE, 223, Short.MAX_VALUE)
                            .addComponent(jpfPassword)
                            .addGroup(jdlLoginLayout.createSequentialGroup()
                                .addComponent(jbtLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                .addContainerGap(139, Short.MAX_VALUE))
        );
        jdlLoginLayout.setVerticalGroup(
            jdlLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jdlLoginLayout.createSequentialGroup()
                .addGap(70, 70, 70)
                .addGroup(jdlLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jdlLoginLayout.createSequentialGroup()
                        .addGroup(jdlLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(jtfUsername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jpfPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel4))
                .addGap(18, 18, 18)
                .addGroup(jdlLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbtLogin)
                    .addComponent(jButton1))
                .addGap(18, 18, 18)
                .addComponent(jlbError, javax.swing.GroupLayout.DEFAULT_SIZE, 89, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jdlWebviewLayout = new javax.swing.GroupLayout(jdlWebview.getContentPane());
        jdlWebview.getContentPane().setLayout(jdlWebviewLayout);
        jdlWebviewLayout.setHorizontalGroup(
            jdlWebviewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jdlWebviewLayout.setVerticalGroup(
            jdlWebviewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBounds(new java.awt.Rectangle(0, 0, 1200, 700));

        cards.setLayout(new java.awt.CardLayout());

        jMenu1.setText("jMenu1");
        jMenuBar.add(jMenu1);

        setJMenuBar(jMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cards, javax.swing.GroupLayout.DEFAULT_SIZE, 800, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cards, javax.swing.GroupLayout.DEFAULT_SIZE, 600, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jbtLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtLoginActionPerformed
        // TODO add your handling code here:
        String username = jtfUsername.getText();
        String password = String.valueOf(jpfPassword.getPassword());
        if (username.length() < 3 || password.length() < 3) {
            jlbError.setText("<html>Tài khoản hoặc mật khẩu phải lớn hơn hoặc bằng 3 kí tự</html>");
        } else {
            this.userInfo = DatabaseHelper.getUserInfo(username, password);
            if (userInfo == null) {
                jlbError.setText("<html>Tài khoản hoặc mật khẩu không chính xác</html>");
            } else {
                SystParams.setCurruentUser(userInfo);
                this.funcGroups = DatabaseHelper.getUserFunctionGroup(userInfo.getId());
                jdlLogin.setVisible(false);
            }
        }
    }//GEN-LAST:event_jbtLoginActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                MainFrame mainFrame = new MainFrame();
                SystParams.setMainFrame(mainFrame);
                mainFrame.setLocationRelativeTo(null);
                mainFrame.login();
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel cards;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar;
    private javax.swing.JButton jbtLogin;
    private javax.swing.JDialog jdlLogin;
    private javax.swing.JDialog jdlWebview;
    private javax.swing.JLabel jlbError;
    private javax.swing.JPasswordField jpfPassword;
    private javax.swing.JTextField jtfUsername;
    // End of variables declaration//GEN-END:variables
}
